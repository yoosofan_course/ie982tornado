import tornado.web

class SimpleHtml:

    def beginHtml():
        return """<!DOCTYPE html> <html><head><title>aaa</title></head><body>"""

    def endHtml():
        return """ </body>   </html>"""

class MainHandler(tornado.web.RequestHandler):

    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        with open("02.01.html", "rt", encoding = "utf8") as f1:
            s1=f1.read()
        self.write(s1);

    def post(self):
        self.write(SimpleHtml.beginHtml()+"MainHandler post <br />");
        self.write(SimpleHtml.endHtml());

class AliHandler(tornado.web.RequestHandler):

    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        if arg1 == "iiii":  self.render("02.03.html", myName=arg1);
        else: self.render("02.01.html")

    def post(self,arg1):
        self.render("02.03.html", myName = self.get_argument('mainInp'));

application = tornado.web.Application([(r"/", MainHandler),(r"/ali/(\w*)", AliHandler),])
application.listen(8000); tornado.ioloop.IOLoop.instance().start()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
