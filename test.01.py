import psycopg2
import momoko
from tornado.ioloop import IOLoop
ioloop = IOLoop.instance()

dsn = "dbname=pc user=postgres password=mypass host=127.0.0.1 port=5432"
conn = momoko.Pool(dsn=dsn)
future = conn.connect()
ioloop.add_future(future, lambda x: ioloop.stop())
ioloop.start()
future.result()  # raises exception on connection error

future = conn.execute("SELECT 1")
ioloop.add_future(future, lambda x: ioloop.stop())
ioloop.start()
cursor = future.result()
rows = cursor.fetchall()
for m1 in rows:
  print(m1)
