import tornado.web
class MainHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.write("""<!DOCTYPE html><html><head><title>aaa</title></head><body>
            GET in MainHandler <a href="/ali/">testLink </a></body></html>
            """);
    def post(self):
      self.write("""<!DOCTYPE html><html><head><title>Ali</title></head><body>
                    MainHandler post
                    </body></html>
                 """);
class AliHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "AliHandler" ')
        self.write("""<!DOCTYPE html><html><head><title>Ali</title></head><body>
            ALI get in AliHandler   <a href="/ali/">testLink </a>
            <form action="/" method="post">
                <input name="a1" value="12" /> 
                <button type="submit" >ddd</button>
            </form>     </body>       </html>
            """);
if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/ali/", AliHandler),
    ])
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
