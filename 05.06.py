import sqlite3
import tornado.web
class dbClass:
    def __init__(self,dbName):
        self.conn = sqlite3.connect(dbName)
        self.c = self.conn.cursor()   
class BaseHandler(tornado.web.RequestHandler):
    def initialize(self,db1):
        self.db=db1
    def get_current_user(self):
        return self.get_secure_cookie("user")
class MainHandler(BaseHandler):
    def get(self): # GET http
        if not self.current_user:
            self.redirect("/login")
            return
        print(' in "get" of "MainHandler" ')
        self.render("05.011.html",myName="aaa",myNewTitle="وب‌گاه من");
        self.finish()
    def post(self):
        if not self.current_user:
            self.redirect("/login")        
        print(' in "post" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");
class AliHandler(BaseHandler):
    def get(self,arg1): # GET http
        if not self.current_user:
            self.redirect("/login")        
        print(' in "get" of "AliHandler" ')
        if arg1=="iiii":  self.render("02.03.html",myName=self.current_user);
        else: self.render("02.01.html")
    def post(self,arg1):
        if not self.current_user:
            self.redirect("/login")                
        print(' in "post" of "AliHandler" ')
        self.render("02.03.html",myName=self.get_argument('mainInp'));

class DbHandler(BaseHandler):
    def get(self): # GET http
        if not self.current_user:
            self.redirect("/login")        
        r1=self.db.c.execute('select year,name from book;')
        self.render('04.05.templates.if.html', allV=r1,myTitle="template.if",myTempVar="<tr></tr>");
        #c.execute("insert into book values('21','حسنی به خانه نرسید',1390);")
class LoginHandler(BaseHandler):
    def get(self):
        self.render('05.06.html');
    def post(self):
        self.set_secure_cookie("user", self.get_argument("name"))
        self.redirect("/")        
class LogoutHandler(BaseHandler):
    def get(self):
        self.set_secure_cookie('user','');
        self.redirect('/');
class ajax01Handler(BaseHandler):
    def get(self,arg1):
        self.write("new value iii"+str(arg1))
        self.finish();
if __name__ == "__main__":
    db1=dbClass("library.sqlite")
    application = tornado.web.Application([
        (r"/", MainHandler, dict(db1=db1)),
        (r"/ali/(\w*)", AliHandler,dict(db1=db1)),
        (r"/db", DbHandler,dict(db1=db1)),
        (r"/login", LoginHandler,dict(db1=db1)),
        (r"/logout", LogoutHandler,dict(db1=db1)),
        (r"/ajax/([0-9]+)", ajax01Handler,dict(db1=db1)),
        (r"/files/tornado", tornado.web.RedirectHandler,
            dict(url="http://127.0.0.1/tornado.pdf")),
    ],cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=")
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()
    conn.commit()
    c.close()
    conn.close()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
