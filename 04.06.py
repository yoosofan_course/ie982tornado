import sqlite3
import tornado.web
class dbClass:
    def __init__(self,dbName):
        self.conn = sqlite3.connect(dbName)
        self.c = self.conn.cursor()   
        print("open database")
class BaseHandler(tornado.web.RequestHandler):
    def f1(self): pass
class MainHandler(BaseHandler):
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.render("04.06_get.html",myName="aaa",myNewTitle="وب‌گاه من");
    def post(self):
        print(' in "post" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");

class AliHandler(BaseHandler):
    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        if arg1=="iiii":  self.render("02.03.html",myName=arg1);
        else: self.render("02.01.html")
    def post(self,arg1):
        print(' in "post" of "AliHandler" ')
        self.render("02.03.html",myName=self.get_argument('mainInp'));

class DbHandler(BaseHandler):
    def initialize(self,db1):
        self.db=db1
    def get(self): # GET http
        print("DbHandler get")
        r1=self.db.c.execute('select year,name from book;')
        self.render('04.06.html', allV=r1,myTitle="template.if",);
        #c.execute("insert into book values('21','حسنی به خانه نرسید',1390);")
        
if __name__ == "__main__":
    myDb=dbClass("library.sqlite")
    try:
        application = tornado.web.Application([
            (r"/", MainHandler),
            (r"/ali/(\w*)", AliHandler),
            (r"/db", DbHandler,dict(db1 = myDb)),
            (r"/files/tornado", tornado.web.RedirectHandler,
                dict(url="http://127.0.0.1/tornado.pdf")),
        ])
        application.listen(8000)
        tornado.ioloop.IOLoop.instance().start()
    except: print("except")
    finally:
        myDb.conn.commit()
        myDb.c.close()
        myDb.conn.close()
        print("close all");

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/

"""
      except KeyboardInterrupt: print("Ctrl+c pressed"); res['flag']=False; res['data']['dbgMem']=self.dbgMem; res['data']['fault']='KeyboardInterrupt'
      except Exception as exc:
        print('67::f2:: except Exception:: before fut2.cancel();');
        print("Error in",exc)
        res['flag']=False; res['data']['fault']='exception'; res['data']['exc']=exc; res['data'][ 'exc_info']= sys.exc_info();#(type, value, traceback)
        res['data']['dbgMem']=self.dbgMem
        traceback.print_exc();

  except ValueError:


  def __init__(self,ydb):
    self.ydb=ydb;    self.ef=self.ydb.ef
  def __enter__(self):#http://docs.quantifiedcode.com/python-anti-patterns/correctness/exit_must_accept_three_arguments.html
    return self
  def __exit__(self, exc_type, exc_value, traceback):#http://book.pythontips.com/en/latest/context_managers.html
    self.ydb.commit(lastCommit=True)
    if exc_type is not None:
      msg= 'exc_type= '  + str(exc_type)+'\n'
      msg+='exc_value= ' + str(exc_value)+'\n'
      self.ef.report(18,msg,traceback)
      if exc_type is KeyboardInterrupt:return False;#http://stackoverflow.com/questions/15344002/re-assign-exception-from-within-a-python-exit-block
    return True

  def __init__(self, ossdb, ossProcessDebugFile, dbgMem): 
    self.ossdb=ossdb; 
    self.ossProcessDebugFile=ossProcessDebugFile; 
    self.dbgMem=dbgMem; 
    self.ossdb.begin()
    self.ossdb.listOfDays=[]
  def __enter__(self): return self #http://docs.quantifiedcode.com/python-anti-patterns/correctness/exit_must_accept_three_arguments.html
  def __exit__(self, exc_type, exc_value, traceback):#http://book.pythontips.com/en/latest/context_managers.html
    if exc_type is not None:
      msg= '::18:: exc_type= '  + str(exc_type)+'\n'
      msg+='exc_value= ' + str(exc_value)+'\n'
      self.ossProcessDebugFile.print(msg,exc_traceback=traceback)
      self.dbgMem.append(msg)#,exc_traceback=traceback)
      print('before rollback::',msg)
      self.ossdb.rollback()
      if exc_type is KeyboardInterrupt:return False;#http://stackoverflow.com/questions/15344002/re-assign-exception-from-within-a-python-exit-block
    else:self.ossdb.commit()

"""
