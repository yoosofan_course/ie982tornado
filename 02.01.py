import tornado.web
class SimpleHtml:
    def beginHtml():
        return """<!DOCTYPE html> <html><head><title>aaa</title></head><body>"""
    def endHtml():
        return """ </body>   </html>"""
class MainHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        with open("02.01.html","rt",encoding="utf8") as f1:
            s1=f1.read()
        self.write(s1);
    def post(self):
        self.write(SimpleHtml.beginHtml()+"MainHandler post <br />");
        self.write(SimpleHtml.endHtml());
class AliHandler(tornado.web.RequestHandler):
    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        self.write(SimpleHtml.beginHtml()+""" ALI  <a href="/ali/rrtr">"""+
            arg1+""" </a><form action="/" method="post">
                <input name="a1" value="12" /> <button type="submit" >ddd</button></form>
            """+SimpleHtml.endHtml());
    def post(self):
        self.write(SimpleHtml.beginHtml()+'post of Ali handler'+SimpleHtml.endHtml());
application = tornado.web.Application([(r"/", MainHandler),(r"/ali/(\w*)", AliHandler),])
application.listen(8000)
tornado.ioloop.IOLoop.instance().start()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
