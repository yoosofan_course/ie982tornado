#!/usr/bin/python3
# Add @tornado.web.authenticated
import sqlite3,os,pathlib
import tornado.web
class dbClass:
    def __init__(self,dbName):
        self.conn = sqlite3.connect(dbName)
        self.c = self.conn.cursor()
class BaseHandler(tornado.web.RequestHandler):
    def initialize(self,db1):
        self.db=db1
    def get_current_user(self):
        return self.get_secure_cookie("user")
class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.render("05.011.html",myName="aaa",myNewTitle="وب‌گاه من");
        self.finish()
    @tornado.web.authenticated
    def post(self):
        print(' in "post" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");
class AliHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        if arg1=="iiii":  self.render("02.03.html",myName=self.current_user);
        else: self.render("02.01.html")
    @tornado.web.authenticated
    def post(self,arg1):
        print(' in "post" of "AliHandler" ')
        self.render("02.03.html",myName=self.get_argument('mainInp'));

class DbHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self): # GET http
        r1=self.db.c.execute('select year,name from book;')
        self.render('04.05.templates.if.html', allV=r1,myTitle="template.if",myTempVar="<tr></tr>");
        #c.execute("insert into book values('21','حسنی به خانه نرسید',1390);")
class LoginHandler(BaseHandler):
    def get(self):
        self.render('06.01.login.html');
    def post(self):
        self.set_secure_cookie("user", self.get_argument("name"))
        self.redirect("/")
class LogoutHandler(BaseHandler):
    def get(self):
        self.set_secure_cookie('user','');
        self.redirect('/');
class ajax01Handler(BaseHandler):
    @tornado.web.authenticated
    def get(self,arg1):
        self.write("new value iii"+str(arg1))
        self.finish();
if __name__ == "__main__":
    settings = {
    "cookie_secret": "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
    "login_url": "/login",
    "static_path": os.path.dirname(os.path.abspath(__file__)),#os.path.dirname(__file__),
    }
    print(pathlib.Path('.').absolute())
    db1=dbClass("library.sqlite")
    application = tornado.web.Application([
        (r"/", MainHandler, dict(db1=db1)),
        (r"/ali/(\w*)", AliHandler,dict(db1=db1)),
        (r"/db", DbHandler,dict(db1=db1)),
        (r"/login", LoginHandler,dict(db1=db1)),
        (r"/logout", LogoutHandler,dict(db1=db1)),
        (r"/ajax/([0-9]+)", ajax01Handler,dict(db1=db1)),
        (r"/files/tornado", tornado.web.RedirectHandler,
            dict(url="http://127.0.0.1/tornado.pdf")),
    ],**settings)
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()
    conn.commit()
    c.close()
    conn.close()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
