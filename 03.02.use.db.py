import sqlite3
import tornado.web

conn = sqlite3.connect('library.sqlite')
c = conn.cursor()

class SimpleHtml:

    def beginHtml():
        return """<!DOCTYPE html> <html><head><title>aaa</title></head><body>"""

    def endHtml():
        return """ </body>   </html>"""

class MainHandler(tornado.web.RequestHandler):

    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");

    def post(self):
        self.write(SimpleHtml.beginHtml()+"MainHandler post <br />");
        self.write(SimpleHtml.endHtml());

class AliHandler(tornado.web.RequestHandler):

    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        if arg1=="iiii":  self.render("02.03.html",myName=arg1);
        else: self.render("02.01.html")

    def post(self,arg1):
        self.render("02.03.html",myName=self.get_argument('mainInp'));

class DbHandler(tornado.web.RequestHandler):

    def get(self): # GET http
        SimpleHtml.beginHtml()
        r1=c.execute('select year,name from book;')
        self.write('<table border="2px">');
        for m1 in r1:
            self.write('<tr>');
            for k1 in m1:
                self.write('<td>'+str(k1)+'</td>')
            self.write('</tr>');
        self.write('</table>')
        SimpleHtml.endHtml()
        #c.execute("insert into book values('21','حسنی به خانه نرسید',1390);")
        
if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/ali/(\w*)", AliHandler),
        (r"/db", DbHandler),
    ])
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()
    conn.commit()
    c.close()
    conn.close()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
