import tornado.web
class MainHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        s= """<!DOCTYPE html> <html><head><title>aaa</title></head><body>"""
        s+="""get mainHandler<a href="/ali/">testLink </a></body></html>""";
        self.write(s)
class AliHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "AliHandler" ')
        self.write("""
            <!DOCTYPE html> 
            <html><head><title>Ali</title></head>
            <body>
            ALI
            <a href="/ali/">testLink </a>
            </body>
            </html>
            """);
application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/ali/", AliHandler),
])
application.listen(8000)
tornado.ioloop.IOLoop.instance().start()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
