import tornado.web
class SimpleHtml:
    def beginHtml():
        return """<!DOCTYPE html> <html><head><title>aaa</title></head><body>"""
    def endHtml():
        return """ </body>   </html>"""
class MainHandler(tornado.web.RequestHandler):
    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.write(SimpleHtml.beginHtml()+"""
            sdjfld
            <a href="/ali/maryampmb">testLink </a>
            <form action="/ali/marpmb" name="testMain" method="post">
            <input name="mainInp" value="45" />
            <button type="submit" name="ss"> submit </button> </form>
            """+SimpleHtml.endHtml());
    def post(self):
        self.write(SimpleHtml.beginHtml()+"MainHandler post <br />");
        self.write(self.get_argument('a1')+SimpleHtml.endHtml());

class AliHandler(tornado.web.RequestHandler):
    def get(self,remUrl1,remUrl2): # GET http
        print(' in "get" of "AliHandler" ')
        self.write(SimpleHtml.beginHtml()+"""
            ALI
            <a href="/ali/ampmb">testLink </a>
            <form action="/" method="post">
                <input name="a1" value="12" /> 
                <button type="submit" >ddd</button>
            </form>
            """+str(remUrl1)+'<br />'+str(remUrl2)+SimpleHtml.endHtml());
    def post(self,remUrl1,remUrl2):
        self.write(SimpleHtml.beginHtml()+'post of Ali handler'+SimpleHtml.endHtml());
        
if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/ali/(\w+)p([a-zA-Z0-9]*)", AliHandler),  # /ali/(\w+)p([a-zA-Z0-9]*)
    ])  # regular expression \w  https://www3.ntu.edu.sg/home/ehchua/programming/howto/Regexe.html
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
