function getAjaxReq(url,affected_id){
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", url);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200){
      document.getElementById(affected_id).innerHTML=xhttp.responseText
    return false;
    };
  }
  xhttp.send(null);
  return false;
}
function postAjaxReq(url,affected_id,formName){
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", url);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200){
      document.getElementById(affected_id).innerHTML=xhttp.responseText
    return false;
  };
 }
 xhttp.send(new FormData(document.getElementById(formName)));
return false;
}
