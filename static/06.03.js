function getAjaxReq(url,affected_id,method="GET"){
  var xhttp = new XMLHttpRequest();
  xhttp.open(method, url);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200){
      document.getElementById(affected_id).innerHTML=xhttp.responseText
    return false;
  };
 }
 xhttp.send(null);
 return false;
}

function getHtml(AjaxType,url,affected_id,formName,callback,validateData,callbackInput=undefined,type=true){
  //let loadelement = document.getElementById('loading-div');
  //if(loadelement !=null)   loadelement.style.display = 'block';
  if(globalAjaxFunction.scriptAlreadyAdded === undefined) globalAjaxFunction.scriptAlreadyAdded
  if(formName===undefined)formName='';if(callback===undefined)callback=noActionOrPass;if(validateData===undefined)validateData=noActionOrPass;
  prep1=validateData()
  if(prep1.state!== true)  return false;
  AjaxType=AjaxType.toUpperCase();
  var xhttp = new XMLHttpRequest();
  xhttp.open(AjaxType, url, type);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200){
      document.getElementById(affected_id).innerHTML=xhttp.responseText
      //http://stackoverflow.com/questions/2158075/jquery-evaluate-script-in-ajax-response
      var scriptElements = document.getElementById(affected_id).getElementsByTagName('script');
      for(i = 0; i < scriptElements.length; i ++){
        var scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        //http://www.ejeliot.com/blog/109
        //http://stackoverflow.com/a/7293377/886607
        //scriptElement.onload = callback;
        if (!scriptElements[i].src)scriptElement.innerHTML = scriptElements[i].innerHTML;
        else{
            scriptElement.src = scriptElements[i].src;
            if(globalAjaxFunction.scriptAlreadyAdded.indexOf(scriptElements[i].src) == -1)
              globalAjaxFunction.scriptAlreadyAdded.push(scriptElements[i].src)
        }
        document.head.appendChild(scriptElement);
      }
      //setTimeout(function(){ if (document.getElementById('loading-div') != null) document.getElementById('loading-div').style.display = 'none'}, 20);
      if(callbackInput == undefined)      
        callback();
      else
        callback(callbackInput)
    }
    return false;
  };
  if(AjaxType=='POST'){
    xhttp.setRequestHeader("X-XSRFToken", getCookie("_xsrf"));
    //httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    if(formName !='')xhttp.send(new FormData(document.getElementById(formName)));
    else{
      //va1="firstname="+fn+"&lastname="+ln
      //var params = "var=" + encodeURIComponent("1");
      //params=params.replace(/%20/g, '+');
      xhttp.send(prep1.data);
    }
  }else{xhttp.send(null);}
  return false;
}

function jsonAsyncData(AjaxType,url,formName,callback,validateData,second_parameteres_optional=undefined,async=true){var prep1;
  if(formName     === undefined) formName    = '';  if(callback     === undefined) callback    = noActionOrPass;  if(validateData === undefined) validateData= noActionOrPass;
  prep1=validateData();if(prep1.state!== true)  return false;
  
  let loadelement = document.getElementById('loading-div');
  if(loadelement !=null)   loadelement.style.display = 'block';  
  
  AjaxType=AjaxType.toUpperCase();   var xhttp = new XMLHttpRequest();
  xhttp.open(AjaxType, url, async);
  xhttp.onreadystatechange=function(){
    if (xhttp.readyState == 4 && xhttp.status == 200){

      let loadelement = document.getElementById('loading-div');
      if(loadelement !=null)   loadelement.style.display = 'none';
      
      if(second_parameteres_optional == undefined)
        callback(xhttp.responseText)
      else callback(xhttp.responseText,second_parameteres_optional)
    }
  };
  if(AjaxType=='POST'){
    xhttp.setRequestHeader("X-XSRFToken", getCookie("_xsrf"));
    if(formName !=''){xhttp.send(new FormData(document.getElementById(formName)));}
    else  xhttp.send(prep1.data);

  }
  else xhttp.send(null);
  return false;
}

function jsonAsyncDataReturnData(AjaxType,url,formName,async=true){var prep1;
  if(formName     === undefined) formName    = '';
    
  let loadelement = document.getElementById('loading-div');
  if(loadelement !=null)   loadelement.style.display = 'block';  
  var data = ''
  AjaxType=AjaxType.toUpperCase();   var xhttp = new XMLHttpRequest();
  xhttp.open(AjaxType, url, async);
  xhttp.onreadystatechange=function(){
    if (xhttp.readyState == 4 && xhttp.status == 200){

      let loadelement = document.getElementById('loading-div');
      if(loadelement !=null)   loadelement.style.display = 'none';
      
      data = JSON.parse(xhttp.responseText)
    }
  };
  if(AjaxType=='POST'){
    xhttp.setRequestHeader("X-XSRFToken", getCookie("_xsrf"));
    if(formName !=''){xhttp.send(new FormData(document.getElementById(formName)));}
    else  xhttp.send(prep1.data);

  }
  else xhttp.send(null);
  return data;
}

