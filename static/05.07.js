async function getAjaxReq(url, affected_id){
    try {
        let response = await fetch(url);  // response is a promise
        // if( response.ok ) then_do
        document.getElementById(affected_id).innerHTML = await response.text();
    } catch (error) {
        console.error(error);
    }
}
async function getAjaxReq2(url, affected_id){
    fetch(url)
        .then(response => response.text())
        .then(stg => { document.getElementById(affected_id).innerHTML = stg; })
        .catch(error => console.error(error));
}
/*
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
https://github.com/whatwg/fetch/pull/585/commits/5550bf95fe2d1ca224f608f2fab1e34e95ea5339
https://github.com/whatwg/fetch
https://github.com/tektoncd/dashboard/pull/599
https://github.com/web-platform-tests/wpt
https://github.com/whatwg/meta/blob/master/MAINTAINERS.md
https://streams.spec.whatwg.org/#rs-class
* 
https://www.xul.fr/en/html5/fetch.php
https://www.xul.fr/html5/fetch.php
* 
https://davidwalsh.name/fetch
* 
https://javascript.info/fetch 
https://github.com/javascript-tutorial/en.javascript.info 
https://github.com/javascript-tutorial/server
*
ajax fetch text get github -jquery

*/

function noActionOrPass(){return {state:true};}

async function getHtml(url, affected_id, AjaxType = 'GET', formName = '',
        callback = noActionOrPass,validateData = noActionOrPass, 
        callbackInput = undefined, type = true)
{
    if(validateData().state !== true) 
        return false;
    AjaxType=AjaxType.toUpperCase();
    if(AjaxType == 'GET'){
        try {
            const response = await fetch(url, {
                    method: AjaxType,
                    headers: {}
            });
            const json = await response.json();
            document.getElementById(affected_id).innerHTML=JSON.stringify(json)
            let scriptElements = document.getElementById(affected_id).getElementsByTagName('script');
            for(i = 0; i < scriptElements.length; i++)
            {
                let scriptElement = document.createElement('script');
                scriptElement.type = 'text/javascript';
                if (! scriptElements[i].src)
                    scriptElement.innerHTML = scriptElements[i].innerHTML;
                else
                {
                    scriptElement.src = scriptElements[i].src;
                    if(globalAjaxFunction.scriptAlreadyAdded.indexOf(scriptElements[i].src) == -1)
                        globalAjaxFunction.scriptAlreadyAdded.push(scriptElements[i].src)
                }
                document.head.appendChild(scriptElement);
            }
            if(callbackInput == undefined)      
                callback();
            else
                callback(callbackInput)
        }
        catch (error) 
        {
            console.error('Error:', error);
            return false;
        }
    }
    else if(AjaxType == 'POST')
    {
        try 
        {
            if(formName !='') 
                let params = new FormData(document.getElementById(formName));
            else 
                let params = prep1.data;
            const response = await fetch(url, {
                method: AjaxType, // or 'PUT'
                body: unescape(encodeURIComponent(params)),
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded;",
                    "X-XSRFToken" : getCookie("_xsrf")
                }
            });
            const json = await response.json();
            document.getElementById(affected_id).innerHTML=JSON.stringify(json)
            let scriptElements = document.getElementById(affected_id).getElementsByTagName('script');
            for(i = 0; i < scriptElements.length; i++){
                let scriptElement = document.createElement('script');
                scriptElement.type = 'text/javascript';
                if (! scriptElements[i].src)
                    scriptElement.innerHTML = scriptElements[i].innerHTML;
                else{
                    scriptElement.src = scriptElements[i].src;
                    if(globalAjaxFunction.scriptAlreadyAdded.indexOf(scriptElements[i].src) == -1)
                        globalAjaxFunction.scriptAlreadyAdded.push(scriptElements[i].src)
                }
                document.head.appendChild(scriptElement);
            }
            if(callbackInput == undefined)      
                callback();
            else
                callback(callbackInput)
        }catch (error) {
            console.error('Error:', error);
            return false;
        }
    }
    return false;
}

async function jsonAsyncData(url, AjaxType = 'GET', formName = '', callback = noActionOrPass,
    validateData = noActionOrPass, second_parameteres_optional = undefined, async = true){
    //let prep1 = validateData(); if(prep1.state !== true) return false;
    AjaxType=AjaxType.toUpperCase();
    if(AjaxType=='GET'){
      try {
        const response = await fetch(url, {
          method: AjaxType,
          headers: {}
        });
        const json = await response.json();
      
        if(second_parameteres_optional == undefined)
          callback(JSON.stringify(json))
        else callback(JSON.stringify(json),second_parameteres_optional)
      }catch (error) {
        console.error('Error:', error);
        return false;
      }
    }
    if(AjaxType=='POST'){
      try {
        var params = "form_name="+formName +"&"+"data="+second_parameteres_optional['data'] ;
        const response = await fetch(url, {
          method: AjaxType, // or 'PUT'
          body: unescape(encodeURIComponent(params)),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            "X-XSRFToken" : getCookie("_xsrf")
          }
        });
        const json = await response.json();
        
        let loadelement = document.getElementById('loading-div');
        if(loadelement !=null)   loadelement.style.display = 'none';
        
        if(second_parameteres_optional == undefined)
          callback(JSON.stringify(json))
        else callback(JSON.stringify(json),second_parameteres_optional)
      }catch (error) {
        console.error('Error:', error);
        return false;
      }

    }
}

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    /*mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer', // no-referrer, *client
    body: JSON.stringify(data) // body data type must match "Content-Type" header*/
  });
  //return await response.json(); // parses JSON response into native JavaScript objects
  return await response.text(); 
}

function getAjaxReq_old(url,affected_id){
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", url);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200){
      document.getElementById(affected_id).innerHTML=xhttp.responseText
    return false;
  };
 }
 xhttp.send(null);
 return false;
}
