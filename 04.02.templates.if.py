import sqlite3
import tornado.web

conn = sqlite3.connect('library.sqlite')
c = conn.cursor()

class MainHandler(tornado.web.RequestHandler):

    def get(self): # GET http
        print(' in "get" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");

    def post(self):
        print(' in "post" of "MainHandler" ')
        self.render("02.04.html",myName="aaa",myNewTitle="وب‌گاه من");

class AliHandler(tornado.web.RequestHandler):

    def get(self,arg1): # GET http
        print(' in "get" of "AliHandler" ')
        if arg1=="iiii":  self.render("02.03.html",myName=arg1);
        else: self.render("02.01.html")

    def post(self,arg1):
        print(' in "post" of "AliHandler" ')
        self.render("02.03.html",myName=self.get_argument('mainInp'));

class DbHandler(tornado.web.RequestHandler):

    def get(self): # GET http
        r1=c.execute('select year,name from book;')
        self.render('04.02.templates.if.html', allV=r1,myTitle="template.if");
        #c.execute("insert into book values('21','حسنی به خانه نرسید',1390);")
        
if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/ali/(\w*)", AliHandler),
        (r"/db", DbHandler),
    ])
    application.listen(8000)
    tornado.ioloop.IOLoop.instance().start()
    conn.commit()
    c.close()
    conn.close()

#http://www.tutorialsavvy.com/2013/11/getting-started-with-tornado-web-framework.html/
